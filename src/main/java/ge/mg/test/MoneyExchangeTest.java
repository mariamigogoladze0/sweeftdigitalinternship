package ge.mg.test;

import ge.mg.MoneyExchange;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoneyExchangeTest {
    @Test
    public void testMinSplit(){
        assertEquals(-1,MoneyExchange.minSplit(-1));
        assertEquals(0,MoneyExchange.minSplit(0));
        assertEquals(1,MoneyExchange.minSplit(5));
        assertEquals(2,MoneyExchange.minSplit(6));
        assertEquals(3,MoneyExchange.minSplit(7));
        assertEquals(4,MoneyExchange.minSplit(4));
        assertEquals(5,MoneyExchange.minSplit(9));
        assertEquals(6,MoneyExchange.minSplit(300));
        assertEquals(7,MoneyExchange.minSplit(301));

    }
}

package ge.mg.test;

import ge.mg.IsProper;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsProperTest {
    @Test
    public void testIsProperly() {
        assertFalse(IsProper.isProperly("(()))()"));
        assertTrue(IsProper.isProperly("(())()"));
        assertTrue(IsProper.isProperly("()"));
        assertTrue(IsProper.isProperly(""));
        assertFalse(IsProper.isProperly("("));
        assertFalse(IsProper.isProperly(")()(()()"));
        assertFalse(IsProper.isProperly("(())())"));

    }
}

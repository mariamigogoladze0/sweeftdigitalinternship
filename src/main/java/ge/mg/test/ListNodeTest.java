package ge.mg.test;

import ge.mg.LinkedList;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ListNodeTest {
    @Test
    public void testListNode() {
        LinkedList head = new LinkedList(1);
        LinkedList index0 = head;
        LinkedList index1 = new LinkedList(2, index0, null);
        index0.setNext(index1);
        LinkedList index2 = new LinkedList(3, index1, null);
        index1.setNext(index2);
        LinkedList index3 = new LinkedList(4, index2, null);
        index2.setNext(index3);
        LinkedList index4 = new LinkedList(5, index3, null);
        index3.setNext(index4);


        head = index4.remove(head);
        assertEquals("[1,2,3,4]", LinkedList.toString(head));
        head = index0.remove(head);
        assertEquals("[2,3,4]", LinkedList.toString(head));
        head = index2.remove(head);
        assertEquals("[2,4]", LinkedList.toString(head));

    }
}

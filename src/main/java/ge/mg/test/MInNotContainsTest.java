package ge.mg.test;

import ge.mg.MinNotContains;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MInNotContainsTest {
    @Test
    public void testNotContains() {
        assertEquals(1, MinNotContains.notContains(new int[]{2, 3, 5}));
        assertEquals(2, MinNotContains.notContains(new int[]{1, 3, 5}));
        assertEquals(3, MinNotContains.notContains(new int[]{1, 2, 5}));
        assertEquals(7, MinNotContains.notContains(new int[]{1, 2, 3, 4, 5, 6}));
    }
}

package ge.mg.test;

import ge.mg.RSSFeed;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;


public class RSSFeedTest {

    @Test
    public void syncFx() throws IOException {
        RSSFeed rssFeed = new RSSFeed();
        double usdGel = rssFeed.exchangeRate("USD", "GEL");
        double gelUsd = rssFeed.exchangeRate("GEL", "USD");
        double usdEUR = rssFeed.exchangeRate("USD", "EUR");
        double eurUsd = rssFeed.exchangeRate("EUR", "USD");

        assertEquals(1, rssFeed.exchangeRate("GEL", "GEL"), 0);
        assertEquals(1, rssFeed.exchangeRate("USD", "USD"), 0);


    }
}

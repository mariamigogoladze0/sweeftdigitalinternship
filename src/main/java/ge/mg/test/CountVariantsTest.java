package ge.mg.test;

import ge.mg.CountVariants;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountVariantsTest {
    @Test
    public void testIsProperly() {
        CountVariants countVariant = new CountVariants();
        assertEquals(-1, countVariant.countVariants(0));
        assertEquals(1, countVariant.countVariants(1));
        assertEquals(2, countVariant.countVariants(2));
        assertEquals(3, countVariant.countVariants(3));
    }
}

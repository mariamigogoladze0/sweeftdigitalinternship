package ge.mg.test;

import ge.mg.Palindrome;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PalindromeTest {
    @Test
    public void testIsPalindrome(){
        assertFalse(Palindrome.isPalindrome("gela"));
        assertTrue(Palindrome.isPalindrome("ana"));
    }
}

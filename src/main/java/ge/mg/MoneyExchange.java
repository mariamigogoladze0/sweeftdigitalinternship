package ge.mg;

public class MoneyExchange {
    public static int minSplit(int amount) {
        if(amount<0){
            return -1;
        }
        int[] coins = {1, 5, 10, 20, 50};
        int count = 0;
        int i = coins.length - 1;
        while (amount > 0) {
            if (amount >= coins[i]) {
                count += amount / coins[i];
                amount = amount % coins[i];
            }
            --i;
        }
        return count;
    }

}

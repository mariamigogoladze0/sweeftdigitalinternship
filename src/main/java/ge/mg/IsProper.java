package ge.mg;

import java.util.Stack;

public class IsProper {
    public static boolean isProperly(String sequence) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < sequence.length(); ++i) {
            char curr = sequence.charAt(i);
            if (curr == '(') {
                stack.push(curr);
            } else {
                if(stack.isEmpty()){
                    stack.add(curr);
                    continue;
                }
                if (stack.peek() == curr) {
                    stack.add(curr);
                } else {
                    stack.pop();
                }
            }
        }
        return stack.size() == 0;
    }
}

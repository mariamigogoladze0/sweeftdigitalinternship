package ge.mg;

import java.util.Arrays;

public class MinNotContains {
    public static int notContains(int[] array){
        if(array.length<1){
            return 1;
        }
        Arrays.sort(array);
        if(array[0]>1){
            return 1;
        }
        for(int i=0; i<array.length-1; ++i){
            if(array[i]+1<array[i+1]){
                return array[i]+1;
            }
        }
        return array[array.length-1]+1;
    }
}

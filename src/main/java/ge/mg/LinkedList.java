package ge.mg;

public class LinkedList {
    int val;
    LinkedList next;
    LinkedList prev;

    public LinkedList(int val) {
        this.val = val;
    }

    public LinkedList(int val, LinkedList prev, LinkedList next) {
        this.val = val;
        this.prev = prev;
        this.next = next;
    }

    public static String toString(LinkedList node) {
        StringBuilder result = new StringBuilder("[");
        while (node != null) {
            result.append(node.getVal()).append(",");
            node = node.next;
        }
        result.deleteCharAt(result.toString().length() - 1);
        result.append("]");

        return result.toString();
    }

    public LinkedList remove(LinkedList head) {
        //delete one
        if (head.next == null && head.prev == null) {
            head = null;
        } else if (this.prev == null) { //delete first
            head = this.next;
            head.prev = null;

        } else if (this.next == null) { //delete last
            this.prev.next = null;
        } else {                        // delete middle
            LinkedList next = this.next;
            LinkedList prev = this.prev;
            prev.next = next;
            next.prev = prev;
        }
        return head;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public LinkedList getNext() {
        return next;
    }

    public void setNext(LinkedList next) {
        this.next = next;
    }

    public LinkedList getPrev() {
        return prev;
    }

    public void setPrev(LinkedList prev) {
        this.prev = prev;
    }
}

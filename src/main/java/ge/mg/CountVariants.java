package ge.mg;

public class CountVariants {
    int[] steps = {1, 2};
    int result;

    public int countVariants(int steersCount) {
        if (steersCount < 0 || steersCount == 0) {
            return -1;
        }
        result = 0;

        findWay(steersCount);
        return result;
    }

    void findWay(int steersCount) {
        if (steersCount == 0) {
            ++result;
        }
        if (steersCount < 0) {
            return;
        }

        for (int step : steps) {
            findWay(steersCount - step);
        }
    }
}

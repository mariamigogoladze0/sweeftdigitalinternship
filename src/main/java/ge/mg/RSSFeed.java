package ge.mg;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;


public class RSSFeed {
    Map<String, Double> currencyMap = new TreeMap<>();

    public double exchangeRate(String from, String to) throws IOException {
        if (from.equals(to)) {
            return 1;
        }
        syncFx();

        if (from.equals("GEL")) {
            double toRate = currencyMap.get(to);
            return 1 / toRate;
        } else if (to.equals("GEL")) {
            return currencyMap.get(from);
        }

        double toGelRage = currencyMap.get(from);
        double fromGelRage = currencyMap.get(to);


        return toGelRage / fromGelRage;
    }

    private void syncFx() throws IOException {
        URL url = new URL("http://www.nbg.ge/rss.php/");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(url.openStream()));

        String inputLine;
        StringBuilder fxRatesAsHtmlStringBuilder = new StringBuilder("");
        while ((inputLine = in.readLine()) != null) {
            fxRatesAsHtmlStringBuilder.append(inputLine);
        }
        in.close();

        String fxRatesAsHtml = fxRatesAsHtmlStringBuilder.toString();

        currencyMap.put("EUR", getCurrencyToGelValueFromHtml(fxRatesAsHtml, "EUR"));
        currencyMap.put("USD", getCurrencyToGelValueFromHtml(fxRatesAsHtml, "USD"));
        currencyMap.put("RUB", getCurrencyToGelValueFromHtml(fxRatesAsHtml, "RUB"));
        currencyMap.put("GBP", getCurrencyToGelValueFromHtml(fxRatesAsHtml, "GBP"));
    }

    private Double getCurrencyToGelValueFromHtml(String fxNbgHtmlTable, String currency) {
        Document doc = Jsoup.parse(fxNbgHtmlTable);
        Node tableNode = doc.childNodes().get(1).childNodes().get(1).childNodes().get(0).childNodes().get(0).childNodes().get(10).childNodes().get(6).childNodes().get(0);
        String tableString = tableNode.toString().substring(tableNode.toString().indexOf("<![CDATA[") + "<![CDATA[ ".length() - 1, tableNode.toString().lastIndexOf("]]>")).trim();
        Elements tables = Jsoup.parse(tableString).select("table");
        for (Element table : tables) {
            Elements trs = table.select("tr");
            for (Element tr : trs) {
                Elements tds = tr.select("td");
                Element td = tds.get(0);
                String parsedCurrency = td.toString().replace("<td>", "").replace("</td>", "");
                if (parsedCurrency.equals(currency)) {
                    Element val = tds.get(2);
                    Element name = tds.get(1);
                    String parsedValue = val.toString().replace("<td>", "").replace("</td>", "");
                    String parsedName = name.toString().replace("<td>", "").replace("</td>", "");
                    return new BigDecimal(parsedValue).divide(new BigDecimal(parsedName.split(" ")[0])).doubleValue();
                }
            }
        }
        return null;
    }
}

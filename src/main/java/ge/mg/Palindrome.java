package ge.mg;

public class Palindrome {
    public static boolean isPalindrome(String input) {
        int i = 0;
        int j = input.length() - 1;
        while (i < j) {
            if (input.charAt(i) != input.charAt(j)) {
                return false;
            }
            ++i;
            --j;
        }
        return true;
    }
}
